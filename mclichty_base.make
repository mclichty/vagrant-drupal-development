core = 7.34
api = 2

projects[drupal][type] = core

; Contrib modules.
projects[ctools][subdir] = "contrib"
projects[entity][subdir] = "contrib"
projects[entityreference][subdir] = "contrib"
projects[features][subdir] = "contrib"
projects[pathauto][subdir] = "contrib"
projects[subpathauto][subdir] = "contrib"
projects[token][subdir] = "contrib"
projects[views][subdir] = "contrib"
projects[views_bulk_operations][subdir] = "contrib"

; Dev modules.
projects[devel][subdir] = "dev"
projects[diff][subdir] = "dev"
projects[features_orphans][subdir] = "dev"
projects[masquerade][subdir] = "dev"
projects[shield][subdir] = "dev"

; Base Theme
projects[bootstrap] = "3.0"
